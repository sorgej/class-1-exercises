// JSCRIPT 200 Class 1

//#1
const card1 = Math.floor(Math.random() * Math.floor(14));
const card2 = Math.floor(Math.random() * Math.floor(14));
const card3 = Math.floor(Math.random() * Math.floor(14));

console.log(card1);
console.log(card2);
console.log(card3);

//#2
maxCard = Math.max(card1,card2, card3);
console.log(maxCard);

//#3
const pizza1Area = (13 / 2 )** 2 * Math.PI;
console.log(pizza1Area);

const pizza2Area = (17 / 2 )** 2 * Math.PI;
console.log(pizza2Area);

//#4
const pizza1Cost = (16.99/pizza1Area);
const pizza2Cost = (19.99/pizza1Area);
console.log(pizza1Cost)
console.log(pizza2Cost);

//
//Address Line
//#1
var firstName;
var lastName;
var streetAddress;
var city;
var state;
var zipCode;

let fullAddress = "firstName lastName\nstreetAddress\ncity, state zipCode";

//#2 Address Extraction
let exampleAddress = "John Snow\n123 4th Ave N\nSeattle Town, CO 98115";
console.log(exampleAddress);

const space1Index = exampleAddress.indexOf(' ');
console.log(space1Index);
firstName = exampleAddress.slice(0,space1Index);
console.log(firstName);
const space2Index = exampleAddress.indexOf('\n');
lastName = exampleAddress.slice(space1Index+1,space2Index);
console.log(lastName);
const space3Index = exampleAddress.indexOf('\n', space2Index+1);
streetAddress = exampleAddress.slice(space2Index+1, space3Index);
console.log(streetAddress);
const space4Index = exampleAddress.indexOf(',',space3Index+1);
city = exampleAddress.slice(space3Index+1, space4Index);
console.log(city);
const space5Index = exampleAddress.indexOf(' ',space4Index+2);
state = exampleAddress.slice(space4Index+2, space5Index);
console.log(state);
// const space6Index = exampleAddress.indexOf(' ',space5Index+2);
zip = exampleAddress.slice(space5Index+1, exampleAddress.length);
console.log(zip);



//
//Find the Middle Date
var date1 = new Date(2019, 0);
var date2 = new Date(2019, 3)
console.log(date1);
console.log(date2);
var timeDiff = Math.abs(date2.getTime() - date1.getTime());
var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
var halfDays = diffDays / 2;

var middleDate = new Date(2019, 0, halfDays);
console.log(middleDate);
